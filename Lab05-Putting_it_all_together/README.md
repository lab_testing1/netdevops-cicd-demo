# Lab 05 - Putting it all together

Now we get to the REALLY good stuff!  This is really what it is all about!  Let's actually configure some network 
connectivity using our pipeline.  For this lab we will be configuring BGP between the POD router and the CORE router.  
This becomes somewhat trivial once we consider how to do it via Ansible/CI pipeline and it can be scaled up to support 
hundreds or thousands of routers.

## Table of Contents

[[_TOC_]]

## Topology Review

![Lab Topology](../images/Lab_Topology.jpg)

The topology has the necessary information to complete your BGP peering.   Your POD device is connected 
to the CORE router with the IP subnet 10.10.1.0/30.  Your AS number is 650XX where XX is replaced with your POD number 
Use a 2 digit number if you have a POD assignment between 1 and 9 (eg. 01, 02, 03, etc).
You will need to peer with 10.10.1.2 in BGP AS65000.  Once the peering occurs, the CORE router will send three routes into the 
POD routing table, 1.0.0.0/8, 2.0.0.0/8, and 3.0.0.0/8.  The INTERNET router has loopback addresses 1.0.0.1, 2.0.0.1, 
and 3.0.0.1 configured.  

[Back to Top](#table-of-contents)

## Creating folder structure 

To add configuration to the pipeline we need to add the necessary variables into the file structure.  In a production 
environment this would ideally by dynamically assigned to Ansible but in the lab we will statically define the variables in 
a host specific variable file.  

To get started we first need to create the proper file and folder structure so that Ansible knows where to find the variables.  
Let's follow our Issue and Merge process to properly define and configure this series of steps. 

Go to your issue log and look for the Issue titled **Configure BGP on POD router to peer for Internet routes** and click on it. 
Read through the instructions to confirm if any additional information not included above is in the ticket.   

Click the **Create a merge request** button to start the process.  On the New merge request screen, notice the branches 
identified at the top of the screen show the auto created branch for this merge request.  The merge is in draft status.  Click the **Assign to me** 
link to assign the issue to yourself and click **Create merge request**.

Go to Repository -> Files and change to the new branch, 1-configure-bgp-on-pod-router-to-peer-for-internet-routes, and click 
on the Lab05 folder. Select the + at the top of the repository to create a new directory.  Name this directory **host_vars** 
and click create directory.  Notice a file .gitkeep is create automatically.  This is a default behavior of Gitlab which 
instructs it to not delete the folder because it is empty.  Ignore the file and create a new folder with the same name as 
your router in the Inventory file of Ansible.  It needs to match exactly so go back to the root directory and look at 
inventory.yml if you do not remember.  It should be **podXX** where XX is the POD assignment.  From the 
Lab05-Putting_it_all_together/host_var directory, create a new folder with the POD assignment, eg. pod01.

## Adding Ansible Variables

Go ahead and create a new file in the Lab05-Putting_it_all_together/host_var directory by clicking the + sign by the path.  
The filename is arbitrary, but for simplicity sake, name it bgp.yml.  Copy (using the copy button) and paste the snippet below 
into the IDE.  Replace the XX in AS number, router_id address, and network address to match your POD assignment.     


Look at the snippet below for a sample of the BGP configuration.

```yaml
---
global_bgp_settings:
  as_number: "650XX"
  bgp:
    router_id:
      address: 10.1.1.X
  networks:
    - address: 10.10.1.0
      netmask: 255.255.255.252
    - address: 10.1.1.X
      netmask: 255.255.255.255
  neighbor:
    - address: 10.10.1.2
      remote_as: "65000"
```

Add the commit message of 'Adding BGP playbook' and click **Commit changes**.  This takes care of the playbook variables.    

## Add Ansible Playbook

Now that the variables are defined, let's write the playbook for BGP.  From the Repository -> Files -> Lab05-Putting_it_all_together/ 
directory, make sure you are in the ```1-configure-bgp-on-pod-router-to-peer-for-internet-routes``` branch and click the 
+ sign to create a new file.  The filename, again arbitrary but useful to follow some descriptive standard, should be 
**configure_bgp.yml**.  Copy and paste the following snippet using the copy button into the text editor.

```yaml
---
- name: Configure BGP on all devices
  hosts: all
  gather_facts: false
  tasks:
    - name: Purge BGP from all devices
      cisco.ios.ios_bgp_global:
        state: purged
      notify: bgp_purged
      register: purged_output

    - name: Configure global BGP for all PODS and CORE
      cisco.ios.ios_bgp_global:
        config: "{{ global_bgp_settings }}"
        state: replaced
      notify: bgp_done
      register: global_output

  handlers:
    - name: Show commands for purging BGP from devices
      listen: bgp_purged
      debug:
        msg: "{{ purged_output.commands }}"
    - name: Show commands for configuring BGP on POD
      listen: bgp_done
      debug:
        msg: "{{ global_output.commands }}"

```

Add a Commit message 'Add BGP playbook' and click **Commit changes**.  

One last thing we need to do before we can edit the CI file to add to our pipeline.

## Add in post test script

The ticket requires us to validate the configuration using a post configuration ping test.  From the Repository -> Files -> 
Lab05-Putting_it_all_together/ directory, make sure you are in the ```1-configure-bgp-on-pod-router-to-peer-for-internet-routes``` 
branch and click the + sign to create a new file.

The filename should be post_bgp_tests.yml.  Copy and paste using the copy button the snippet below into the editor.

```yaml
---
- name: Ping all known Internet IPs
  hosts: all
  gather_facts: false
  tasks:
    - name: Ping all Internet IPs
      cisco.ios.ios_ping:
        dest: "{{ item }}"
        source: loopback1
        count: 2
      loop: 
        - 1.0.0.1
        - 2.0.0.1
        - 3.0.0.1
      register: ping_results

    - name: Display results of ping test for Loopback 1
      debug:
        msg: >
          {% if item.packet_loss == '0%' %} Destination host
          {{ item.invocation.module_args.dest }} - SUCCESS.
          {% else %} Destination host {{ item.invocation.module_args.dest }}
          - FAILED. {% endif %}
      loop: "{{ ping_results.results|flatten(1) }}"
      loop_control:
        label: "{{ item.invocation.module_args.dest }}"
```

NOW we have all the pieces we need to properly add to the pipeline.  We have created the variables, created the playbook 
for both BGP and the ping test.  We are ready to update the pipeline.
 
## Edit .gitlab-ci.yml file

Back in the repository, make sure you have ```1-configure-bgp-on-pod-router-to-peer-for-internet-routes``` branch selected and 
go to the Lab05-Putting_it_all_together/.gitlab-ci.yml file and click **Open in Web IDE**.  Copy the section below using 
the copy button and overwrite everything in the .gitlab-ci.yml file.  

Notice that we have a sleep timer setup for the Verify Internet Routes job.  This is because BGP sometimes takes a bit to 
peer and converge routes.  Once the timer expires it will execute the ansible-playbook to do the ping verification.

```yaml
---
stages:
    - validate
    - deploy
    - verify

image: tcurtis45/ansible-basic:latest

validate YAML:
    stage: validate
    script:
        - cd Lab04-Proper_Issues_and_Merge_Processes
        - bash linter.sh
    tags:
        - docker

validate SNMP Passwords:
    stage: validate
    script:
        - cd Lab04-Proper_Issues_and_Merge_Processes
        - python3 checkpass.py
    tags:
        - docker

Deploy SNMP:
    stage: deploy
    script:
        - >
            ansible-playbook -i inventory.yml
            Lab04-Proper_Issues_and_Merge_Processes/snmp.yml
    tags:
        - docker
    rules:
        - if: $CI_COMMIT_BRANCH == "main"
          changes:
              - Lab05-Putting_it_all_together/snmp.yml
              - Lab05-Putting_it_all_together/group_vars/all.yml

Deploy BGP:
    stage: deploy
    script:
        - >
            ansible-playbook -i inventory.yml
            Lab05-Putting_it_all_together/configure_bgp.yml
    tags:
        - docker
    rules:
        - if: $CI_COMMIT_BRANCH == "main"
          changes:
              - Lab05-Putting_it_all_together/**/bgp.yml
              - Lab05-Putting_it_all_together/configure_bgp.yml

Verify Internet Routes:
    stage: verify
    script:
        - >
          sleep 60
          ansible-playbook -i inventory.yml 
          Lab05-Putting_it_all_together/post_bgp_tests.yml
    rules:
        - if: $CI_COMMIT_BRANCH == "main"
          changes:
              - Lab05-Putting_it_all_together/**/bgp.yml
              - Lab05-Putting_it_all_together/configure_bgp.yml
```

Click **Create commit...**, add in a message, and commit to the 1-configure-bgp-on-pod-router-to-peer-for-internet-routes.  
If you see the box for 'start a new merge request' you can uncheck it and then click **Commit**.

## Review the pipeline

Let's go look at the pipeline and make sure everything looks right. Go to Merge request and open the Draft request.  

![Successful Draft Pipeline](../images/Lab05-Successful_Draft_Pipeline.jpg)

If you look the CI pipeline you will see that it kicked off and should have completed the 
jobs for validate YAML and the validate SNMP Passwords.  We should have the YAML validated but probably didn't need to 
check the SNMP password since nothing changed.  Let's add some rules to go ahead and correct that so that the BGP changes 
execute the BGP playbook and SNMP changes execute the SNMP playbook.

## Add rules for SNMP Password Checker in Pipeline

Once again, go back to the Repository -> Files and change the branch to 1-configure-bgp-on-pod-router-to-peer-for-internet-routes.  

Drill down to Lab05-Putting_it_all_together/.gitlab-ci.yml file and click **Open in Web IDE** to edit.  Copy and paste 
the entire section below using the copy button and replace everything in the file.  

```yaml
---
stages:
    - validate
    - deploy
    - verify

image: tcurtis45/ansible-basic:latest

validate YAML:
    stage: validate
    script:
        - cd Lab05-Putting_it_all_together
        - bash linter.sh
    tags:
        - docker

validate SNMP Passwords:
    stage: validate
    script:
        - cd Lab05-Putting_it_all_together
        - python3 checkpass.py
    tags:
        - docker
    rules:
        - changes:
              - Lab05-Putting_it_all_together/group_vars/all.yml

Deploy SNMP:
    stage: deploy
    script:
        - >
            ansible-playbook -i inventory.yml
            Lab05-Putting_it_all_together/snmp.yml
    tags:
        - docker
    rules:
        - if: $CI_COMMIT_BRANCH == "main"
          changes:
              - Lab05-Putting_it_all_together/snmp.yml
              - Lab05-Putting_it_all_together/group_vars/all.yml

Deploy BGP:
    stage: deploy
    script:
        - >
            ansible-playbook -i inventory.yml
            Lab05-Putting_it_all_together/configure_bgp.yml
    tags:
        - docker
    rules:
        - if: $CI_COMMIT_BRANCH == "main"
          changes:
              - Lab05-Putting_it_all_together/**/bgp.yml
              - Lab05-Putting_it_all_together/configure_bgp.yml

Verify Internet Routes:
    stage: verify
    script:
        - sleep 60
        - >
            ansible-playbook -i inventory.yml
            Lab05-Putting_it_all_together/post_bgp_tests.yml
    rules:
        - if: $CI_COMMIT_BRANCH == "main"
          changes:
              - Lab05-Putting_it_all_together/**/bgp.yml
              - Lab05-Putting_it_all_together/configure_bgp.yml
```

Notice the new rules under the Validate SNMP Passwords only will run if the group_vars/all.yml file is changed since it is 
the only file that contains passwords.  

Click **Create commit...**, make sure the 1-configure-bgp-on-pod-router-to-peer-for-internet-routes branch is selected, and 
click **Commit**.

## Review the pipeline

Go back and review the pipeline one more time.  Go to CI/CD -> Pipelines and look at the pipeline that was just kicked off.  
We should now see only the YAML validation occur as part of the pipeline.  If this is all you see, it is working correctly.  
Now we can merge into ```main``` to see the rest of the pipeline kick off.

## Merge into main

Go to the Merge request and choose the Draft.  Click the button to **Mark as Ready** and add some comments.  
Click the **Merge** button and wait for the pipeline box to update.

Once you see the pipeline kick off, go ahead and click it to watch the status.  If all works well, you will see the validate 
YAML job, Deploy BGP, and Verify Internet Routes.  Wait for the job to complete to validate that BGP was configured and 
is working properly.

![Lab05 - Main Successful Push](../images/Lab05-Successful_Main_Pipeline.jpg)

Click on the Verify Internet Routes job for more details on the ping test results.  The ansible script pinged all three 
IPs and got an OK back, therefore the message says success.  Because the playbook came back successful with no errors, 
this stage of the test has passed. 

![Lab05 - Main Successful Push](../images/Lab05-Successful_Main_Pipeline.jpg)

## Review the artifact

As soon as the job for Verify Internet Routes finishes, the screen does an update and you should see a window on the right side that 
now says **Job Artifacts**.  Based on our configuration in the CI file, this artifact will be kept in the pipeline log for 
1 week and then will be deleted.  Click the **Download** button to download that has our command output attached!  
How cool is that? Imagine the time saved to manually download and save logs when every single pipeline could have an archive of 
standardized commands output. 

![Lab05 - Job Artifacts](../images/Lab05-Job_Artifact.jpg)

## What's next?

Congrats for making it to the end of the lab.  I'll leave you with a few thoughts about how we could use these pipelines 
to make our lives easier, give our customers value, and prove that Insight really is the leading solutions integrator.  We 
are combining multiple automation tools, scripts, containers, and more that work together to provide truly scalable solutions 
to our customers.  As you leave here I have some closing thoughts on how Insight can leverage CI to bring this cutting 
edge technology to our customers since we barely scratched the surface of the capabilities here.

- Integrate with IPAM/DNS for IP assignment, DNS name entry
- Integrate with NMS systems for enabling system or interface monitoring or active polling
- Testing configurations using Batfish or PyATS/Genie
- Running configurations in virtual lab mock-up
- Change control management 
    - Open/Close tickets
    - Notify people of the change
    - Disable monitoring during changes

Those are just a handful of the many ways we can begin to think about using pipelines for automation.  What other ways 
do you think we can use CI automation?  We want to hear your feedback.  Be sure to talk to Tony, Kevin, or Josh about your 
ideas.  We love talking about this stuff.  

## Conclusion

Congrats on making it to the end of the lab.  Our network is configured, our Issues have been closed, and we can go home knowing that the 
network is stable.  As you delve into the power of CI there are many new things to learn: rules, roles, protected branches, security, and much more.  
I hope it has been truly informative and eye opening of the power CI can bring to Insight's network automation development efforts, or 
any development for that matter.  You should now have the ability to explain the value of a CI pipeline, how jobs and stages work, 
applying rules, using Docker containers in your pipeline, and controlling the flow of a pipeline.  Remember that although we focused on 
Gitlab, these same concepts apply to many different CI/CD applications including Azure DevOps, Github, Jenkins and more. Anyone can begin to start using 
automation or collaboration using Gitlab or Github as a common platform.  Gitlab.com is offered free of service for Individual 
users.  This would include your skunkworks projects.  As we start serious use of CI/CD toolsets for marketable software, we will likely standardize 
on one of the platforms mentioned above and buy licensing to support the users that we have.  

Most importantly, now that you have an understanding, start having the conversations with your customers about the power of 
CI/CD.  Talk to your peers, managers, and other leaders.  If you are excited about this stuff, get them excited about it! 
As we look to expand our role as the leading solutions integrator expect to see more and more CI pipelines running more 
and more development efforts not only for us, but for our customers. 

Please reach out to Tony Curtis, Kevin Breit, or Josh Spiers if you have any questions about the lab or want to demo 
this process again.  Remember the lab will continue to run until 3/24/23.  If you want to run it after that time, please 
reach out to one of us to coordinate access.  


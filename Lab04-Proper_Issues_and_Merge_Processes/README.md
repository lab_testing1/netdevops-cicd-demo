# Lab04 - Proper Issues and Merge Processes

In this lab we will focus on proper git workflow.  It is important to have a sound workflow for any production development or automation.
  Workflow provides structure to ensure that changes that are made in development are not released directly into production 
  without proper testing/validation, and possibly staging.  Git workflow also allows us to share our development with peers.  
  Multiple developers could be working on different parts of the code at the same time and following workflow will ensure they 
  are not overwriting each others changes. 
  
**Objective**: Understand and demonstrate how to follow Gitlab workflow by looking at issues, branching code, making changes, 
merging, and approving code into the ```Main``` branch. 

## Table of Contents

[[_TOC_]]

## What is a Git workflow?

Git has been around since April 18, 2005 written by Linus Torvalds (creator of Linux).  No open source system met his needs 
for source-control so he decided to write his own.  While git has changes in various forms since it's original inception, many of the 
same core version control concepts still exist.  One of which is the idea of branching and merging code.  

Everyone knows that we shouldn't make network changes in production and the same applies for software development.  Version control 
allows us to create a branch of code from our 'main' branch that is an exact copy from the date of branch but which can be 
changed and ran independent from the branch it was created from.  This allows us to create staging, development, testing, or 
whatever branch in our source code that is required for business needs. 

Gitlab offers a streamlined workflow for a typical git process.  Look at the diagram below for an example of this workflow.

>![Gitlab workflow](../images/Lab04-Gitlab_workflow.jpg)

Following this workflow, changes always start with an Issue in the main branch.  A merge request is created to branch the 
changes into a unique workflow.  Once changes have been made and committed, the CI pipeline and validated.  Typically a peer review 
or manual approval would happen at this phase, and then the issue is closed and merged into the main branch.  The CI pipeline 
is kicked off again and runs against the main branch which has production equipment/software in it.  The administrator then 
needs to validate/verify that the changes occurred as expected.

[Back to Top](#table-of-contents)

## Review starting files

Notice that we have all the starting files that we ended Lab03 with plus one extra file, checkpass.py.  The script is shown below.  
No modification is needed but is provided for your information.  This script uses only standard packages so should be able to
 run on any system with Python3 installed.  
The script will open the file group_vars/all.yml and look for any line that says snmp_password.  If it finds that line it 
will split it to remove the password after the :, and then run that password through a complexity function to determine that 
it meets minimum requirements (1 upper, 1 lower, 1 number, 1 special character, 8 characters long). 

It's not necessary to understand what we are doing here, it is just provided as informational.

```python
import sys
import re


def main():
    passwords = get_passwords("group_vars/all.yml")
    bad_pass = False
    for password in passwords:
        password_results = password_check(password)
        if not password_results['password_ok']:
            print(f"SNMP string security check failed for string {password}")
            bad_pass = True
    # If results come back false, one of the test have failed.  Report the error
    if bad_pass:
        print("SNMP string security check FAILED. Ensure strings match security standard before resubmitting job.")
        sys.exit(1)
    else:
        print("SNMP string security check PASSED.")
        sys.exit(0)


def password_check(password):
    """
    Verify the strength of 'password'
    Returns a dict indicating the wrong criteria
    A password is considered strong if:
        8 characters length or more
        1 digit or more
        1 symbol or more
        1 uppercase letter or more
        1 lowercase letter or more
    """

    # calculating the length
    length_error = len(password) < 8

    # searching for digits
    digit_error = re.search(r"\d", password) is None

    # searching for uppercase
    uppercase_error = re.search(r"[A-Z]", password) is None

    # searching for lowercase
    lowercase_error = re.search(r"[a-z]", password) is None

    # searching for symbols
    symbol_error = re.search(r"[ !#$%&'()*+,-./[\\\]^_`{|}~"+r'"]', password) is None

    # overall result
    password_ok = not (length_error or digit_error or uppercase_error or lowercase_error or symbol_error)

    return {
        'password_ok': password_ok,
        'length_error': length_error,
        'digit_error': digit_error,
        'uppercase_error': uppercase_error,
        'lowercase_error': lowercase_error,
        'symbol_error': symbol_error,
    }


def get_passwords(filename):
    # Look for global variable file
    # Blank list for tracking found passwords
    snmp_strings = []
    with open(filename, 'r') as variable_file:
        lines = variable_file.readlines()
        for row in lines:
            # check if SNMP string is present
            word = 'snmp_password'
            # Using the find method, search for the value.  -1 will be returned if not found
            if row.find(word) != -1:
                # Split the string to grab the password value
                split_string = row.split(":")[1].strip()
                snmp_strings.append(split_string)
    return snmp_strings


main()

```

**Review .gitlab-ci.yml file**

Our `.gitlab-ci.yml` file is exactly the same as the end of Lab03. Notice we are set to use the Docker image still.

## Issue List

To view the issue list, look for Issues -> List on the toolbar on the left side of Gitlab.  

You should see two issues in the list, SNMP password complexity, and BGP peer information.  We will be working on the SNMP 
password complexity in this lab.

Click on the Issue for the SNMP Password complexity and review the issue.

>![Lab04 - Issue List](../images/Lab04-Password_complexity_issue.jpg){width=800}

Click the Create merge request button to create a new branch and start a new merge request.

##  Create merge request

After clicking the create merge request button you will be taken to the New merge request screen.  From here you can write 
notes, assign users, reviewers, etc to the merge request.  Go ahead and add the following to the notes.

```
Adding SNMP string complexity checker to CI pipeline for Lab04.
```

Scroll down and select **Create merge request**.

Notice the merge request is still in Draft notated in the title of the Merge request.  
You will also see there are no changes in the merge request.

>![Lab04 - Draft merge request](../images/Lab04-SNMP_Complexity_Draft_Merge.jpg){width=800}

Let's go edit a file in the newly created branch shown at the top of the window as indicated in the screenshot above.

## Edit the files in the new branch

For sake of simplicity, the previous step made a complete copy of the files located in the ```main``` branch and copied them to a new branch. 
Since the name was auto-created and taken from the merge request title, it should be something similar 
to ```1-snmp-password-complexity-and-pipeline-modification```. 

To access the new branch, go to the main repository.  Click on the repository drop down as shown in the screenshot below and 
choose the new branch. All the files should match at this point.

>![Lab04 - Draft merge request](../images/Lab04-Merge_branch_repository.jpg){width=800}

Open the folder for Lab04-Proper_Issues_and_Merge_Processes and open the `.gitlab-ci.yml` file in the web IDE using the 
**Open Web IDE** button.

Replace the content of the file with everything copied from the box below.

```yaml
---
stages:
    - validate
    - deploy

image: tcurtis45/ansible-basic:latest

validate YAML:
    stage: validate
    script:
        - cd Lab04-Proper_Issues_and_Merge_Processes
        - bash linter.sh
    tags:
        - docker

validate SNMP Security:
    stage: validate
    script:
        - cd Lab04-Proper_Issues_and_Merge_Processes
        - python3 checkpass.py
    tags:
        - docker

Deploy SNMP:
    stage: deploy
    script:
        - ansible-playbook -i inventory.yml Lab04-Proper_Issues_and_Merge_Processes/snmp.yml
    tags:
        - docker
``` 
As soon as the content changes, the blue **Create commit** button should color in.  Click it.  

Add a comment to the Commit Message box and commit to 1-snmp-password-complexity-and-pipeline-modification branch.  Uncheck the box
 for 'Start a new merge request' if it pops up (since we have already created a merge request in a previous step) and click **Commit**.

[Back to Top](#table-of-contents)

## Review the merge request 

On the toolbar, choose Merge requests and open the Draft request we have created.  

Notice that a few things have changed on this screen.  We should see a pipeline running for the commit, an approval button, 
a warning saying the merge is blocked, updated activity, and the numbers beside Commits, Pipelines, and Changes should 
all have numbers beside the names.

>![Lab04 - Merge Request Review](../images/Lab04-SNMP_Merge.jpg)

Click on **Commits** under the title and status to see the commits that are part of this merge.  If you click on the commit listed, 
you will see the lines added in the .gitlab-ci.yml file.  Remember a single merge could have multiple changes in different files.   

Also note that the pipeline was kicked off when we committed the file.  If you view the pipeline by clicking the Pipelines icon at the top, 
you will see 1 or 2 pipelines.  If you see two, you can ignore the longer pipeline as it is an artifact of the lab environment we are in.  
Open the pipeline executed by your most recent change which should be labeled 'Update Lab04-Proper_Issues_and_Merge_Processes'.

The pipeline has failed.         

>![Lab04 - Pipeline Failed](../images/LAB04-CICD_pipeline_failed-1.jpg)

Click on the failed task for Validate SNMP Strings and you see that both the RO and RW string failed the complexity requirement.

## Update SNMP Strings

Since we can make multiple changes in a single merge, let's go back to the branch and update the SNMP configuration to 
meet the complexity requirements.

Go to the repository -> Files.  Make sure you change to the 1-snmp-password-complexity-and-pipeline-modification branch
 and open `Lab04-Proper_Issues_and_Merge_Processes/group_vars/all.yml` with the **Open in Web IDE** option.

Change the SNMP string to meet the complexity requirements.  Update the SNMP strings to what is shown below. 

```yaml
snmp_password_global_ro: RO_SNMP123@abc
snmp_password_global_rw: RW_SNMP123@abc

```

Add a comment to the Commit Message box and commit to 1-snmp-password-complexity-and-pipeline-modification branch.  Click **Commit**.


>![Lab04 - Pipeline Successful](../images/Lab04-Successful%20Pipeline.jpg)

The pipeline should have ran successfully.  Maybe a little too successfully.  Notice that not only did we validate the passwords, 
but we also deployed them to the device as part of the last step?  Per the issue requirements, we only want to run the 
 deploy stage when we merge code into production, the ```main``` branch.

To do this we need to go back to the CI file and specify some rules.

## Rules for CI jobs

Now that we are using multiple branches we may need to control which actions occur at which stage.  In production this may 
mean only working on virtual hosts while doing development, then pilot/lab devices for testing before rolling changes into production.

Go to the repository -> Files.  Make sure you change to the `1-snmp-password-complexity-and-pipeline-modification` branch
 and open `Lab04-Proper_Issues_and_Merge_Processes/.gitlab-ci.yml` with the **Open in Web IDE** option.

Let's add two new rules for the SNMP deployment job:

- It should only run if we merge into the main branch
- It should only run if we update the snmp.yml or the group_vars/all.yml file

Replace the content in `.gitlab-ci.yml` with copied data from below. 

```yaml
---
stages:
    - validate
    - deploy

image: tcurtis45/ansible-basic:latest

validate YAML:
    stage: validate
    script:
        - cd Lab04-Proper_Issues_and_Merge_Processes
        - bash linter.sh
    tags:
        - docker

validate SNMP Passwords:
    stage: validate
    script:
        - cd Lab04-Proper_Issues_and_Merge_Processes
        - python3 checkpass.py
    tags:
        - docker

Deploy SNMP:
    stage: deploy
    script:
        - >
            ansible-playbook -i inventory.yml
            Lab04-Proper_Issues_and_Merge_Processes/snmp.yml
    tags:
        - docker
    rules:
        - if: $CI_COMMIT_BRANCH == "main"
          changes:
              - Lab04-Proper_Issues_and_Merge_Processes/snmp.yml
              - Lab04-Proper_Issues_and_Merge_Processes/group_vars/all.yml

```

Gitlab CI offers many rules for the pipelines.  More information can be found [here](https://docs.gitlab.com/ee/ci/jobs/job_control.html).

For this example we use the if rule to do a logical comparison to the branch.  If the branch is main AND changes are made 
to snmp.yml or group_vars/all.yml and this pipeline will be triggered.

Go ahead and commit these changes and review the pipeline again.

## Review Merge request

Go to the Merge request in the toolbar and open the Draft merge we have created.  Look at the most recent pipeline and click 
on the status icon to verify the status of each stage.  

>![Lab04 - Pipeline Successful](../images/Lab04-Successful%20Pipeline2.jpg)

Now we only see the validate stage as expected since the changes aren't made to the ```main``` branch.

Also look at the changes that are part of this merge.  From the merge request, click on the **Changes** icon at the top to 
see the diff between different files.  You will see both the `Lab04-Proper_Issues_and_Merge_Processes/group_vars/all.yml` and 
`Lab04-Proper_Issues_and_Merge_Processes/.gitlab-ci.yml` changes in the window.  We also see the total number of adds, deletions, and 
the lines are colored for easy identification of changes.

>![Lab04 - Draft Final Changes](../images/Lab04-Draft_Final_Changes.jpg)

To continue on, merge the changes into the ```main``` branch.

## Merge into main branch

Now that we know are changes are correct and validation is passed, we can merge into production by pushing the changes 
into the ```main``` branch.  From the merge request screen, click back on the **Overview** icon at the top.  

Click **Mark as ready** to remove from draft state.  You see the merge details will commit 3 changes to main, source branch will 
be deleted, and the issue will automatically be closed.  Once clicked the box changes to 'Ready to merge!'.  Click the **Merge** 
button to push the changes to ```main```.

## Review pipeline for final merge status

From the merge screen we should now see a new pipeline kick off.  If you don't see it you can go to the CI/CD -> Pipelines to view.

Click the pipeline status and drill down to Lab4_PIPELINE to see the status of each job.  You should see not only the validate 
stages but also the deploy stage are executed and successful. 

## Review Cisco IOS Configuration

To verify the changes actually took effect on the device, SSH into your POD router and type the following command.

```show run | inc snmp-server```

We should see our snmp strings of **RO_SNMP123@abc** and **RW_SNMP123@abc**.  

## Conclusion

Issue creation, merge request, and commits are integral parts ot successful git based development.  This lab shows the power 
of gitlab workflow to accomplish these things.  As a fully integrated CI pipeline and VCS tool Gitlab offers all the components 
necessary to perform this workflow.  Issues that are identified should be tagged with Issue request.  Those issue requests 
should be picked up by engineers/technicians/architects for resolution.  Each issue can be treated as it's own branch/merge 
incident or they can be bundled into the same merge.  

Using rules to define when a job will execute is also critical to the proper implementation of a CI workflow.  Consider at 
what stage or when which actions occur that will trigger a job.  This provides a more robust CI stack, reduces duplication 
of work, ensures critical validation occurs at any stage, and prevents changes from going into production before they are fully tested.

In our next Lab, Lab05-Putting_it_all_together, we will take these same skills and add a BGP configuration to your POD router.  
Go ahead and move onto the next lab, [Lab05-Putting_it_all_together](../Lab05-Putting_it_all_together/README.md).
  
